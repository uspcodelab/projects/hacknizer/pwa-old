################################################################################
#                                  BASE IMAGE                                  #
################################################################################

# Use official Node's alpine image as base
FROM node:10.6-alpine AS base

# Create PWA_PATH if it doesn't exist and set it as the working dir
ENV PWA_PATH=/usr/src/pwa
WORKDIR $PWA_PATH

# Define base environment variables for the PWA
ENV HOST=0.0.0.0 \
    PORT=8000

# Expose default port to connect with the service
EXPOSE $PORT

################################################################################
#                               DEVELOPMENT IMAGE                              #
################################################################################

# Use base image to create the development image
FROM base AS development

# Set development environment development
ENV NODE_ENV=development

# Install system dependencies
RUN apk add --no-cache --repository http://dl-3.alpinelinux.org/alpine/edge/testing \
    vips-tools vips-dev fftw-dev glib-dev build-base

# Copy package.json and package-lock.json
COPY package.json yarn.lock ./

# Install dependencies and create a volume for them
RUN yarn install

# Copy the application code to the build path
COPY . .

# Define default command to execute when running the container
CMD [ "yarn", "run", "dev" ]
