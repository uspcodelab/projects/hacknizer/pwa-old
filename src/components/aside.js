import Link from 'gatsby-link'
import React, { Component } from 'react'

class MenuItem extends Component {
  render() {
    return (
      <Link
        to={this.props.href}
        className="pv3 ph3 mh2 no-underline secondary-color f4 hover-bg-light-gray"
        onClick={this.props.onClickHandler}
      >
        {this.props.name}
      </Link>
    )
  }
}

class Aside extends Component {
  constructor(props) {
    super(props)
    this.logout = this.logout.bind(this)
  }

  logout() {
    localStorage.setItem('token', '')
    this.props.activated = !this.props.activated
  }

  render() {
    let link = (name, href) => {}

    let loginButton = (
      <div className="flex flex-column">
        <MenuItem name="Cadastrar" href="/signup" />
        <MenuItem name="Login" href="/login" />
      </div>
    )
    if (localStorage.getItem('token')) {
      loginButton = (
        <div className="flex flex-column">
          <MenuItem name="Meu Perfil" href="/user" />
          <MenuItem name="Sair" href="/" onClickHandler={this.logout} />
        </div>
      )
    }
    return (
      <div className="aside" data-activated={this.props.activated}>
        <div className="absolute top-0 bottom-0 left-0 w5 z-2">
          <div className="bg-white avenir h-100">
            <div className="avenir bg-primary light-color pl4 pv3 mb3 f2 fw6">
              Hacknizer
            </div>
            <div className="flex flex-column">
              <MenuItem name="Home" href="/" />
              <MenuItem name="Edições" href="#" />
              <hr className="bt-0 bl-0 bb b--moon-gray ma2" />
              {loginButton}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Aside
