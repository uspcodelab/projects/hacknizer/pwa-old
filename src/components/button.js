import React, { Component } from 'react'
import '../styles/main.scss'

export default class Button extends Component {
  constructor(props) {
    super(props)

    this.classes = 'f4 avenir fw4 tc br2 ph3 pv2 dib ma1 ba nowrap'

    if (this.props.style === 'outline')
      this.classes += ' bg-white primary-color b--primary hover-bg-light-gray'
    else this.classes += ' white bg-primary b--primary dim'
  }
  render() {
    return (
      <button
        className={this.classes}
        onClick={this.props.onClick}
        type="button"
      >
        {this.props.text}
      </button>
    )
  }
}
