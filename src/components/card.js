import React, { Component } from 'react'
import '../styles/main.scss'

export default class Card extends Component {
  constructor(props) {
    super(props)

    this.classes = ''
    if (this.props.style === 'list')
      this.classes += 'bt bb b--moon-gray pa3 mv2 bg-light-gray'
    else this.classes += 'br2 bg-light-gray ba b--primary pa3 ma3 h-100'
  }

  render() {
    return <div className={this.classes}>{this.props.children}</div>
  }
}
