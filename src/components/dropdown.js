import React, { Component } from 'react'
import '../styles/main.scss'

export default class Dropdown extends Component {
  constructor(props) {
    super(props)
    if (!this.props.options) this.options = []
    else this.options = this.props.options
  }
  //   addOption() {}
  render() {
    return (
      <select className="bg-light-gray ba b--primary primary-color fw5 pv2 ph3 ma2 br2 outline-transparent">
        {this.options.map(({ value, text }) => (
          <option value={value} key="value">
            {text}
          </option>
        ))}
      </select>
    )
  }
}
