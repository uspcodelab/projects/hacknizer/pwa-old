import React from 'react'

import Header from '../components/header'
import ListItem from '../components/listItem'
import logo from '../images/typhlosion.gif'

class EditionsList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      editions: [
        {
          Hackathon: { name: 'Halloackathon 2018' },
          start_date: '31/10',
          end_date: '01/11',
          Pages: [{ logo: logo }],
        },
        {
          Hackathon: { name: 'HackXmas 2018' },
          start_date: '25/12',
          end_date: '26/12',
          Pages: [{ logo: logo }],
        },
        {
          Hackathon: { name: 'Hackaillon 2018' },
          start_date: '01/01',
          end_date: '02/01',
          Pages: [{ logo: logo }],
        },
      ],
    }
  }

  async componentDidMount() {}

  renderList(list) {
    const newList = list.map(el => (
      <ListItem key={`${el.Hackathon.name}`}>
        <img
          src={el['Pages'][0].logo}
          style={{ width: '50px', height: '50px' }}
        />
        &nbsp;
        <span>{el['Hackathon'].name}</span>&nbsp;
        <span>
          {el.start_date} - {el.end_date}
        </span>
      </ListItem>
    ))
    return <div>{newList}</div>
  }

  render() {
    return (
      <div>
        <Header>Próximos Hackathons</Header>
        {this.renderList(this.state.editions)}
      </div>
    )
  }
}

export default EditionsList
