import React, { Component } from 'react'
import Link from 'gatsby-link'
import IonIcon from 'react-ionicons'

class Footer extends Component {
  render() {
    return (
      <div className="flex flex-column items-center bg-light-gray dark-gray pv3">
        <div className="flex flex-row mv2 underline">
          <a href="#" className="link mh2">
            Termos de Serviço
          </a>
          <a href="#" className="link mh2">
            Privacidade
          </a>
          <a href="#" className="link mh2">
            Cookies
          </a>
        </div>
        <div className="center mv2">
          Made with &nbsp;
          <IonIcon icon="md-heart" color="red" />
          &nbsp; by
          <a href="http://codelab.ime.usp.br" className="link secondary-color">
            &nbsp; USPCodeLab
          </a>
        </div>
      </div>
    )
  }
}

export default Footer
