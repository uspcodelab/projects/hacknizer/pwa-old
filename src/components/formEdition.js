import React from 'react'

class FormEdition extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      startDateEvent: '',
      endDateEvent: '',
      startTimeEvent: '',
      endDateInsc: '',
      endTimeInsc: '',
    }
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(event, value) {
    this.setState({ [event.target.name]: event.target.value })
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          <h2>DATA DE REALIZAÇÃO</h2>
          Início: &nbsp;
          <input
            type="date"
            name="startDateEvent"
            value={this.state.startDateEvent}
            onChange={this.handleChange}
          />
          &nbsp;
          <input
            type="time"
            name="startTimeEvent"
            value={this.state.startTimeEvent}
            onChange={this.handleChange}
          />&nbsp;Fim: &nbsp;
          <input
            type="date"
            name="endDateEvent"
            value={this.state.endDateEvent}
            onChange={this.handleChange}
          />
          &nbsp;
          <input
            type="time"
            name="endTimeEvent"
            value={this.state.endTimeEvent}
            onChange={this.handleChange}
          />
          <h2>DATA DE INSCRIÇÕES</h2>
          Início: &nbsp;
          <input
            type="date"
            name="startDateInsc"
            value={this.state.startDateInsc}
            onChange={this.handleChange}
          />
          &nbsp;
          <input
            type="time"
            name="startTimeInsc"
            value={this.state.startTimeInsc}
            onChange={this.handleChange}
          />&nbsp;Fim: &nbsp;
          <input
            type="date"
            name="endDateInsc"
            value={this.state.endDateInsc}
            onChange={this.handleChange}
          />
          &nbsp;
          <input
            type="time"
            name="endTimeInsc"
            value={this.state.endTimeInsc}
            onChange={this.handleChange}
          />
        </label>
      </form>
    )
  }
}

export default FormEdition
