import React, { Component } from 'react'
import '../styles/main.scss'

export default class Header extends Component {
  render() {
    return (
      <div className="avenir bg-light-gray primary-color f3 fw4 bt bb b--moon-gray pv3 tc">
        {this.props.children}
      </div>
    )
  }
}
