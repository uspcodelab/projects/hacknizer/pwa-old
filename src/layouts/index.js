import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

import Navbar from '../components/navbar'
import Footer from '../components/footer'
import '../styles/main.scss'

class Layout extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    let ConditionalNavbar = null
    let noHeaderPaths = ['/', '/login', '/signup']
    if (!noHeaderPaths.includes(this.props.location.pathname))
      ConditionalNavbar = <Navbar />

    return (
      <div className="flex flex-column body bg-near-white">
        <Helmet
          title={this.props.data.site.siteMetadata.title}
          meta={[
            { name: 'description', content: 'Sample' },
            { name: 'keywords', content: 'sample, something' },
            { name: 'viewport', content: 'width=device-width' },
          ]}
        />
        <div>
          {ConditionalNavbar}
          <div>{this.props.children()}</div>
        </div>
        <Footer />
      </div>
    )
  }
}

Layout.propTypes = {
  children: PropTypes.func,
}

export default Layout

export const query = graphql`
  query SiteTitleQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`
