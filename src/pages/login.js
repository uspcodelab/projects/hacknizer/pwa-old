import React, { Component } from 'react'
import gql from 'graphql-tag'
import { client } from '../../gatsby-browser'

import FormSign from '../components/formSign'
import '../styles/main.scss'

const login = gql`
  mutation funcLogin($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      token
    }
  }
`

class Login extends Component {
  constructor(props) {
    super(props)

    this.loginHandler = this.loginHandler.bind(this)
  }

  async loginHandler({ email, password }) {
    const response = await client.mutate({
      mutation: login,
      variables: {
        email,
        password,
      },
    })
    localStorage.setItem('token', response.data.login.token)
    this.props.history.push('/')
  }

  render() {
    const state = { email: '', password: '' }
    return <FormSign type="login" state={state} handler={this.loginHandler} />
  }
}

export default Login
